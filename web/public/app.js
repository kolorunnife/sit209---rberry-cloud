//const API_URL = 'http://localhost:5001/api';
//const SOCKET_URL = `http://10.140.51.246:3000`;
const SOCKET_URL = `https://rberry-sockets.herokuapp.com`;
const API_URL = `https://rberry-api.herokuapp.com/api`;

$("#side_navbar").load('side_navbar.html');
$("#navbar").load('navbar.html');
$('#footer').load('footer.html');

function ErrorAlert(message) {
    Swal.fire({
        title: 'Error: Invalid Request',
        text: message,
        type: 'error',
        confirmButtonText: 'X'
    });
}

function FancyAlert(title, type, message) {
    Swal.fire({
        title: title,
        text: message,
        type: type,
        confirmButtonText: 'X'
    });
}

//Change Theme Events
$(window).on('resize', function() {
    buttonWidth = $('.file-button').width() - $('.ftp_location').width();
    pageWidth = $('.table-wrap').width();
    $('.ftp_location').width((pageWidth - buttonWidth) + "px");

    $("#editor").width($('.heading').width() + "px");
    $("#editor").height($(document).height() - ($('.heading').height() + $('.file-button').height() + $('.navbar').height() + 50) + "px");
});

$('#register-button').on('click', function() {
    const name = $('#name').val();
    const email = $('#email').val();
    const password = $('#password').val();
    const confirm_password = $('#confpassword').val();

    if (password == confirm_password) {
        $.post(`${API_URL}/registeration`, { name, email, password, confirm_password })
            .then((response) => {
                if (response.success) {
                    location.href = '/';
                } else {
                    ErrorAlert(response.message);
                }
            });
    }
});

$('#login-button').on('click', function() {
    const email = $('#email').val();
    const password = $('#password').val();
    $.post(`${API_URL}/authenticate`, { email, password })
        .then((response) => {
            if (response.success) {
                localStorage.setItem('key', response.key)
                location.href = `/devices?key=${response.key}`;
            } else {
                ErrorAlert(response.message);
            }
        });
});

$("#fileDialogBox").change((e) => {
    const file_input = e.target;

    console.log(file_input.files[0]);

    var dataArray = new FormData();
    dataArray.append('file', file_input.files[0]);
    dataArray.append('deviceId', localStorage.getItem('current-device'));
    dataArray.append('apiKey', localStorage.getItem('current-device'));
    dataArray.append('path', $('#pathViewer').val() + "\\");

    $.ajax({
        url: `${SOCKET_URL}/upload`,
        type: 'POST',
        data: dataArray,
        processData: false, // tell jQuery not to process the data
        contentType: false, // tell jQuery not to set contentType
        success: function(data) {
            console.log(`Upload Reposnse: ${data}`);
        }
    });
});

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

var serverSocket;

function StartSocket() {
    if (localStorage.getItem('current-device-data') == null || localStorage.getItem('current-device-status') == null) {
        console.log("Please select a device");
        return;
    }

    serverSocket = io(SOCKET_URL);

    const clientSocket = io(`${SOCKET_URL}/${localStorage.getItem('key')}`);

    clientSocket.on('device_status', (data) => {
        if (data.success) {
            alert('Device is online');
        }
    });

    clientSocket.on('client_forward', (data) => {
        data = JSON.parse(data);
        const type = data.type;

        $('.status_icon').attr('status', 'online');

        if (type == 'disconnect_status' && localStorage.getItem('current-device') == data.value) {
            $('.status_icon').attr('status', 'offline');
            window.location.href = '/';
            return;
        }

        if (type == 'device-alert' && data.isEvent != true) {
            console.log(data);
            FancyAlert('Hey, User!', data.alert_type, data.value);

            if (data.alert_type == 'success') {
                $('#fileList').html('');

                SendMsg(deviceId, {
                    type: 'request_directory',
                    path: $('#pathViewer').val()
                });
            }

            return;
        }

        //If at dashboard
        if (window.location.pathname == '/dashboard') {
            if (type == 'cpu') {
                SetChartPercent(cpuChart, [data.value], "%");
            } else if (type == 'ram') {
                SetChartPercent(ramChart, [data.value], "%");
            } else if (type == 'core_temp') {
                SetChartPercent(tempChart, [data.value], "C");
            }
        } else if (window.location.pathname == '/events') {
            if (data.isEvent) {

                var rowCount = $('#events > tbody > tr').length;

                if (rowCount > 1) {
                    $('#events > tbody').remove();
                }

                if (rowCount == 0) {
                    $('#events tbody').append(`
                    <tr data-device-id=${data.device_id}">
                        <td>${data.device_id}</td>
                        <td>${data.type}</td>
                        <td>${data.value}</td>
                        <td>${data.sent_at}</td>
                    </tr>`);
                } else {
                    $('#events tbody').after(`
                    <tr data-device-id=${data.device_id}">
                        <td>${data.device_id}</td>
                        <td>${data.type}</td>
                        <td>${data.value}</td>
                        <td>${data.sent_at}</td>
                    </tr>`);
                }


                console.log(data);
            }
        } else if (window.location.pathname == '/browser') {
            if (type == 'download_file_ready') {
                console.log(data);
                window.open(`${API_URL}/download?key=${localStorage.getItem('key')}`);
            } 
            else if (type == 'edit_file') {
                var mode = "";
                const ext = data.path.split("\\")[data.path.split("\\").length - 1].split(".")[1];

                if (ext == 'py') mode = 'python';
                else if (ext == 'txt') mode = 'text';
                else mode = ext;

                console.log(ext);

                editor.getSession().setMode("ace/mode/" + mode);
                editor.setValue(window.atob(data.value), 1);
                editor.setShowPrintMargin(false);
                $("#saveButton").removeAttr("disabled");
            } 
            else if (type == 'directory_listing' && !data.isEvent) {
                $('#fileList').html('');

                const keys = Object.keys(data.value);

                if (keys.length == 0) {
                    console.log('No results');
                    $('#fileList').append(`
                    <tr>
                        <td></td>
                        <td></td>
                        <td><b>No Results!</b></td>
                        <td></td>
                        <td></td>
                    </tr>`);
                    return;
                }

                keys.forEach(element => {
                    const path = data.value[element];

                    //console.log(path);

                    if (path.type == 'folder') {
                        $('#fileList').append(`
                        <tr folder path-data="${element}">
                            <td><span class="iconify" data-icon="feather-folder" data-inline="true" style="display: inline"></span></td>
                            <td>${element}</td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="button" class="btn btn-warning btn-sm download-file" data="${element}" onclick="deleteFile('${element}');">
                                    <span class="iconify" data-icon="feather-trash" data-inline="true" style="display: inline"></span>
                                </button>
                            </td>
                        </tr>`);
                    } else if (path.type == 'file') {
                        $('#fileList').append(`
                        <tr file path-data="${element}">
                            <td><span class="iconify" data-icon="feather-file-text" data-inline="true"></span></td>
                            <td>${element}</td>
                            <td>${new Date(path.last_modified * 1000).toLocaleString("en-US")}</td>
                            <td>${bytesToSize(path.size)}</td>
                            <td>
                                <button type="button" class="btn btn-info btn-sm edit-file" data="${element}" onclick="editFile('${element}');">
                                    <span class="iconify" data-icon="feather-edit" data-inline="true" style="display: inline"></span>
                                </button>

                                <button type="button" class="btn btn-primary btn-sm download-file" data="${element}" onclick="downloadFile('${element}');">
                                    <span class="iconify" data-icon="feather-download" data-inline="true" style="display: inline"></span>
                                </button>

                                <button type="button" class="btn btn-danger btn-sm download-file" data="${element}" onclick="deleteFile('${element}');">
                                    <span class="iconify" data-icon="feather-trash" data-inline="true" style="display: inline"></span>
                                </button>
                            </td>
                        </tr>`);
                    }
                });
            }
        }



    });
}

StartSocket();

function SendMsg(id, data) {
    serverSocket.emit('device_forward', JSON.stringify({ device_id: id, api_key: localStorage.getItem('key'), value: data }));
    console.log(`Emitting: ${data}`);
}

$('#button-addon2').click(() => {
    // const confirm = prompt('What would you like the name to be?');

    // if (confirm != null && confirm.length > 3){
    //     window.open(`/api/download?key=${localStorage.getItem('key')}&name=${confirm}`);
    // }

});