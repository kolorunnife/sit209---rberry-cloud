const express = require('express');
const CloudManager = require('./manager');
const mongoose = require('mongoose');
const app = express();
const fs = require('fs');
const port = process.env.PORT || 80;
const base = `${__dirname}/public`;
const passport = require('passport'), FacebookStrategy = require('passport-facebook').Strategy;

app.use(passport.initialize());
app.use(passport.session());

passport.use(new FacebookStrategy({
    clientID: 455364075066940,
    clientSecret: '0a74e11fde2165155b493ab17331f39b',
    callbackURL: "http://localhost:3000/auth/facebook/callback"
},
    function (accessToken, refreshToken, profile, done) {
        return done(null, profile);
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});


app.get('/auth/facebook', passport.authenticate('facebook', { session: false }));

app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }),
    function (req, res) {
        console.log(req.user.displayName);
        res.send(`
            <script>
                localStorage.setItem('name', '${req.user.displayName}');
                localStorage.setItem('isAuthenticated', true);
                window.location.href = 'http://localhost:3000/dashboard';
            </script>
        `);
});

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept-Type');
    next();
});

app.use(express.static('public'));

mongoose.connect(process.env.MONGO_URL);

//Web Listener
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

app.get('/', (req, res) => {
    res.sendFile(`${base}/login.html`);
});

app.get('/dashboard', async(req, res) => {
    //Display error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.sendFile(`${base}/404.html`);
        return;
    }

    res.sendFile(`${base}/dashboard.html`);
});

app.get('/settings', async(req, res) => {
    //Sisplay error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.redirect('/login');
        return;
    }

    res.sendFile(`${base}/settings.html`);
});

app.get('/devices', async(req, res) => {
    //Display error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.sendFile(`${base}/404.html`);
        return;
    }

    res.sendFile(`${base}/device_list.html`);
});

app.get('/events', async(req, res) => {
    //Display error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.sendFile(`${base}/404.html`);
        return;
    }

    res.sendFile(`${base}/events.html`);
});

app.get('/browser', async(req, res) => {
    //Display error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.sendFile(`${base}/404.html`);
        return;
    }

    res.sendFile(`${base}/file_browser.html`);
});

app.get('/gpio', async(req, res) => {
    //Display error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.sendFile(`${base}/404.html`);
        return;
    }

    res.sendFile(`${base}/GPIOPIN.html`);
});

app.get('/login', async(req, res) => {
    //Redirect to devices page if user has valid api key
    if (await CloudManager.authenicateClient(res, req.query.key)) {
        res.redirect('/devices?key=' + req.query.key);
        return;
    }

    res.sendFile(`${base}/login.html`);
});

app.get('/registration', async(req, res) => {
    //Redirect to devices page if user has valid api key
    if (await CloudManager.authenicateClient(res, req.query.key)) {
        res.redirect('/devices?key=' + req.query.key);
        return;
    }

    res.sendFile(`${base}/registration.html`);
});



app.get('/api/download', async(req, res) => {
    //Display error if user has invalid api key
    if (await CloudManager.authenicateClient(res, req.query.key) == false) {
        res.sendFile(`${base}/404.html`);
        return;
    }

    res.download('../RPi Bridge/bridge.zip');
});

app.get('/download', (req, res) => {
    res.setHeader('Content-disposition', 'attachment; filename=' + "afile.txt");
    res.setHeader('Content-Type', 'text/plain');

    res.send('hi');
});

//Error 404
app.get('*', (req, res) => {
    res.sendFile(`${base}/404.html`);
});