const Devices = require('./models/device');
const Accounts = require('./models/user');
const Downloads = require('./models/download');
const crypto = require('crypto');

module.exports = {

    //Misc
    authenicateClient: async function (res, key) {
        if (await this.apiKeyExists(key) == false) {
            res.send('Invalid permissions');
            return false;
        }

        return true;
    },

    hash: function (secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    downloadExists: async function (key) {
        var promise = Downloads.findOne({ api_key: key }, (err, document) => {
            if (document != undefined && document != null) {}
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    saveDownload: async function (key, name, data) {
        name = name.toString().split("/")[name.toString().split("/").length - 1];

        if (await this.downloadExists(key)) {
            var promise = Downloads.findOne({ api_key: key }, (err, document) => {

                if (document != undefined && document != null) {
                    document.file_name = name;
                    document.contents = data;

                    document.save(err => {
                        if (err) console.log("ERROR: Failed to update device data.", err);
                    });
                }
            });

            const promiseValue = await promise

            return promiseValue == null || promiseValue == undefined ? false : true;
        }
        else {
            const newDownload = new Downloads({
                api_key: key,
                file_name: name,
                contents: data,
            });

            var promise = newDownload.save(err => {
                if (err) {

                }
            });

            const promiseValue = await promise

            return promiseValue == null || promiseValue == undefined ? true : false;
        }
    },

    getDownload: async function (key) {
        var promise = Downloads.findOne({ api_key: key }, (err, document) => {
            if (document != undefined && document != null) { }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? null : promiseValue;
    },

    //Device
    deviceIdExists: async function (key, convert) {

        if (convert) { key = this.hash('', key); }

        var promise = Devices.findOne({ device_id: key }, (err, document) => {

            if (document == null || document == undefined) {
                //console.log('Device does not exist. -> ' + result);
            }
            else {
                //console.log('Device does exist. -> ' + result);
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    updateDevice: async function (device_id, ip, time, name) {

        device_id = this.hash('', device_id);

        var promise = Devices.findOne({ device_id: device_id }, (err, document) => {
            if (document != undefined && document != null) {
                document.ip_address = ip;
                document.last_accessed = time;
                document.name = name;
                document.status = true;

                document.save(err => {
                    if (err) console.log("ERROR: Failed to update device data.", err);
                });
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    updateDeviceStatus: async function (device_id, status) {

        var promise = Devices.findOne({ device_id: device_id }, (err, document) => {
            if (document != undefined && document != null) {
                document.status = status;

                document.save(err => {
                    if (err) console.log("ERROR: Failed to update device data.", err);
                });
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    registerDevice: async function (device_id, name, username, ip, type, last_accessed, status) {
        const newDevice = new Devices({
            device_id: this.hash('', device_id),
            name,
            type,
            username,
            ip_address: ip,
            last_accessed,
            status
        });

        var promise = newDevice.save(err => {
            if (err) {
                success = false;
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    deleteDevice: async function (device_id) {

        var promise = Devices.findOne({ device_id }, (err, document) => {

            if (document != null && document != undefined) {
                document.remove(err => {
                    if (err) {
                        success = false;
                    }
                });
            }
            else {
                success = false;
            }

        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },


    //Users
    apiKeyExists: async function (key) {

        var promise = Accounts.findOne({ api_key: key }, (err, document) => {
            if (document == null || document == undefined) { }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getUsername: async function (key) {

        var promise = Accounts.findOne({ api_key: key }, (err, document) => {
            if (document != undefined && document != null) { }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? ' ' : promiseValue.email;
    },

    getDeviceId: async function (username, name) {

        var promise = Devices.findOne({ username: username, name: name }, (err, document) => {
            if (document != undefined && document != null) {
                result = document.email;
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? ' ' : promiseValue.device_id;
    },
}
