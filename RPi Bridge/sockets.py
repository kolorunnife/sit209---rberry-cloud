import subprocess
import sys
import threading
import time
from datetime import datetime
import os

def install(package):
    if package not in sys.modules.keys():
        subprocess.call(['pip', 'install', package])
        
#Install needed packages
#install('python-socketio[client]')
#install('requests')
#install('json')
#install('environs')
#install('psutil')

import socketio
import requests
import json
import psutil
import cloud

global sio
sio = socketio.Client()
global DEVICE_ID
DEVICE_ID = ''

def send_client_message(sio, key, room, data):
    data.update({'api_key': cloud.API_KEY})
    data.update({'device_id': cloud.DEVICE_NAME})
    data.update({'room' : room})
    data.update({'sent_at' : datetime.now().strftime("%d/%m/%Y %I:%M %p")})
    resp_data = json.dumps(data)

    data.update({'isEvent' : True})
    event_data = json.dumps(data)
    
    sio.emit('client_forward', resp_data, namespace='/')
    sio.emit('client_forward', event_data, namespace='/')
    

def device_details():
    last_cpu_percent = round(psutil.cpu_percent())
    last_ram_percent = round( (psutil.virtual_memory().percent) )
    last_core_temp = 0
    print(psutil.sensors_temperatures())
    print(psutil.sensors_temperatures()['cpu-thermal'][0].current)

    if hasattr(psutil, 'sensors_temperatures'):
        last_core_temp = round(psutil.sensors_temperatures()['cpu-thermal'][0].current)

    #Send Initial Device Details
    data = { 'type': 'cpu', 'value': last_cpu_percent } 
    send_client_message(sio, cloud.API_KEY, 'client_forward', data)
        
    data = { 'type': 'ram', 'value': last_ram_percent } 
    send_client_message(sio, cloud.API_KEY, 'client_forward', data)

    while True:
        cpu_percent = round(psutil.cpu_percent(interval=0.1))
        ram_percent = round( psutil.virtual_memory().percent )
        
        if cpu_percent != last_cpu_percent:
            last_cpu_percent = cpu_percent
            data = { 'type': 'cpu', 'value': cpu_percent }
            
            send_client_message(sio, cloud.API_KEY, 'client_forward', data)
            time.sleep(1)

        if ram_percent != last_ram_percent:
            last_ram_percent = ram_percent
            data = { 'type': 'ram', 'value': ram_percent }
            
            send_client_message(sio, cloud.API_KEY, 'client_forward', data)
            time.sleep(1)

        if hasattr(psutil, 'sensors_temperatures') == False:
            continue

        core_temp = round(psutil.sensors_temperatures()['cpu-thermal'][0].current)

        if core_temp != last_core_temp:
            last_core_temp = core_temp
            data = { 'type': 'core_temp', 'value': core_temp }
            
            send_client_message(sio, cloud.API_KEY, 'client_forward', data)
            time.sleep(1)

def get_directory_listing(path_list):
    path_dict = {}
    subfolders = [f for f in os.scandir(path_list)]
    
    for path in subfolders:
        #print('Path: ' + str(path.stat()))
        if os.path.isfile(path.path):
            path_dict.update({path.name : {'type': 'file', 'size': path.stat().st_size, 'last_modified': path.stat().st_mtime }})

        if os.path.isdir(path.path):
            path_dict.update({path.name : {'type': 'folder'}})

    return path_dict

def get_file_contents(path):
    file = open(path, 'rb', encoding='utf8') 
    text = file.read()
    print(text)
    return text

def manage_event(data):
    TYPE = data['value']['type']
    VALUE = data['value']
    
    if TYPE == 'request_directory':
        dir_path = VALUE['path']
        dir_listing = get_directory_listing(dir_path)
        data = { 'type': 'directory_listing', 'value': dir_listing } 
        return data
    elif TYPE == 'download_file':
        dir_path = VALUE['path']
        
        try:
            content = get_file_contents(dir_path)
        
            post_data = json.dumps({
                'file_name' : dir_path,
                'contents': content,
                'apiKey': cloud.API_KEY
            })
            
            cloud.send_post_request(cloud.API_URL + 'download', post_data)
            data = { 'type': 'download_file', 'value': {'success': True} } 
            return data
        except:
            data = { 'type': 'download_file', 'value': {'success': False} } 
            return data


def connect_to_server(did):
    global DEVICE_ID
    #Socket.IO Listeners
    DEVICE_ID = did
    SOCKET_URL = 'http://localhost:3000'
    #SOCKET_URL = 'https://rberry-sockets.herokuapp.com'
    
    sio.connect(SOCKET_URL, namespaces=['/' + DEVICE_ID], headers={'device_id': did, 'api_key': cloud.API_KEY})
    print('Device ID: ' + DEVICE_ID)

    threading.Thread(target = device_details, args = ()).start()
    print('/%s' %(DEVICE_ID))

    @sio.on('device_forward', namespace='/%s' %(DEVICE_ID))
    def on_message(data):
        data = json.loads(data)
        print(data)
        api_key = data['api_key']
        device_id = data['device_id']
        value = data['value']
        
        resp = manage_event(data)
        send_client_message(sio, api_key, 'client_forward', resp)
    
    @sio.on('ask_status', namespace='/%s' %(DEVICE_ID))
    def on_message(data):
        data = cloud.gen_json(
            ['key', 'room', 'value'],
            [cloud.API_KEY, 'device_status', {}]
        )
            
     


