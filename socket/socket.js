var app = require('express')();
const cors = require('cors');
const fileupload = require('express-fileupload');

app.use(cors());
app.use(fileupload());

const server = require('http').Server(app);
const io = require('socket.io')(server);

const port = process.env.PORT || 3000;
const CloudManager = require('./manager');

console.log("Server will be listening on port " + port);
server.listen(port);

function emit(io, namespace, room, data) {
    if (data.type != "cpu" && data.type != "core_temp"){
        console.log("Emitting: /" + namespace + " #" + room + " : " + JSON.stringify(data));
    }

    io.of('/' + namespace).emit(room, JSON.stringify(data));
}

app.get('/download', function (req, res) {
    const key = req.query.key;

    emit(io, key, 'client_forward', { type: 'download_file_ready', api_key: key});

    return res.send('File ready');
});

app.post('/upload', function (req, res) {
    const { apiKey, deviceId, path } = req.body;
    const contents = req.files.file.data.toString('base64');

    emit(io, deviceId, 'device_forward', {
        api_key: apiKey,
        device_id: deviceId,
        value: {
            type: 'upload_file',
            contents: contents,
            name: req.files.file.name,
            path: path
        }
    });

    return res.send('File is ready for upload.');
});

app.get('/*', function (req, res) {
    return res.send('This socket test is working!');
});

io.on("connection", function (socket) {

    //Device -> Client
    socket.on("client_forward", function (data) {
        data = JSON.parse(data);

        var key = data.api_key;
        var room = data.room;
        var val = data.value;

        //console.log("Device -> Client: " + JSON.stringify(data));

        emit(io, key, room, data);
    });

    //Client -> Device
    socket.on("device_forward", function (data) {
        data = JSON.parse(data);
        var device_id = data.device_id;

        //console.log("Client -> Device: " + JSON.stringify(data));

        emit(io, device_id, 'device_forward', data);
    });

    //Client (Asking for Status) -> Device
    socket.on("ask_status", function (data) {
        var key = data.key;
        var device_id = data.device_id;

        //console.log(data);

        emit(io, device_id, 'ask_status', { api_key: key });
    });

    socket.on('disconnect', async function () {
        const api_key = socket.handshake.headers.api_key;
        const device_id = socket.handshake.headers.device_id;

        if (api_key == undefined || device_id == undefined || api_key == null || device_id == null) return;

        emit(io, api_key, 'client_forward', {
            type: 'disconnect_status',
            value: device_id
        });

        await CloudManager.updateDeviceStatus(device_id, api_key, false);

    });
});