const mongoose = require('mongoose');

module.exports = mongoose.model('Downloads', new mongoose.Schema({
    api_key: String,
    file_name: String,
    contents: String
}));
