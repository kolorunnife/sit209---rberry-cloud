import sockets as sock

import requests
import json
import subprocess
from requests import get
import time
from datetime import datetime
import __main__ as main
from environs import Env
import threading
import socketio

env = Env()
env.read_env()

global ONLINE
ONLINE = True

#API Information
API_URL = 'http://localhost:5001/api/'
#API_URL = 'https://rberry-api.herokuapp.com/api/'
API_KEY = env.str('API_KEY')
IP = get('https://api.ipify.org').text
TYPE = 'Raspberry Pi'

def send_get_request(URL, DATA):
    global ONLINE
    try:
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = get(url = URL, data=DATA, headers=headers)
        # extracting data in json format 
        res = r.json()
        ONLINE = True
        return res
    except:
        ONLINE = False
        return {}

def send_post_request(URL, DATA):
    global ONLINE
    try:
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(url = URL, data=DATA, headers=headers) 
        # extracting data in json format 
        res = r.json()
        ONLINE = True
        return res
    except:
        ONLINE = False
        return {}

def send_put_request(URL, DATA):
    global ONLINE
    try:
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.put(url = URL, data=DATA, headers=headers)
        # extracting data in json format 
        res = r.json()
        ONLINE = True
        return res
    except:
        ONLINE = False
        return {}

def gen_json(keys, values):
    data = {}

    for i in range(0, len(keys)):
        data[keys[i]] = values[i]

    data = json.dumps(data)
    return data

def get_mac_address():
    #HWID = str(subprocess.check_output('wmic csproduct get uuid')).split('\\r\\r\\n')[1].strip()
    #BSN = str(subprocess.check_output('wmic bios get serialnumber')).split('\\r\\r\\n')[1].strip()
    #return HWID + "-" + BSN
    interface = 'wlan0'
    try:
        str = open('/sys/class/net/%s/address' %interface).read()
    except:
        str = "00:00:00:00:00:00"
        
    return str[0:17]

def is_online():
    global ONLINE
    return ONLINE

def ping():
    try:
        resp = send_get_request(API_URL + 'test', {})
        if resp['success']:
            print('Ping was successfull')
    except:
        pass
    
DEVICE_NAME = env.str('DEVICE_NAME')
    
def start_bridge():
    #Register device and ping
    print('Pinging api server for connectivity')
    ping()

    prev_state = 'OFFLINE'
    DEVICE_ID = get_mac_address()

    while True:
        if is_online() and prev_state == 'OFFLINE':
            resp = send_post_request(API_URL + 'devices', gen_json(['deviceId', 'apiKey', 'deviceName', 'ip', 'type'], [DEVICE_ID, API_KEY, DEVICE_NAME, IP, TYPE]))
            print(resp['message'])

            did = send_get_request(API_URL + 'device/' + API_KEY + '/' + DEVICE_NAME, {})['id']
            sock.connect_to_server(did)

            resp = send_put_request(API_URL + 'devices', gen_json(['deviceId', 'apiKey', 'ip', 'last_accessed'],[DEVICE_ID, API_KEY, IP, datetime.now().strftime("%d/%m/%Y %h:%M %p")]))
            print(resp['message'])

            prev_state = 'ONLINE'
            
        elif is_online() == False:
            prev_state = 'OFFLINE'
            print('Not connected to a network. Status: ' + str(ONLINE))
            ping()
            time.sleep(5)

        #Update device ping every 5 secs
        resp = send_put_request(API_URL + 'devices', gen_json(['deviceId', 'apiKey', 'ip', 'last_accessed', 'device_name'],[DEVICE_ID, API_KEY, IP, datetime.now().strftime("%d/%m/%Y %I:%M %p"), DEVICE_NAME]))
        #print(resp['message'])
        time.sleep(3)

#Sub Thread for send http requests
thread = threading.Thread(target = start_bridge, args = ())
thread.start()


