const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
const cors = require('cors');
const app = express();
var server = require('http').Server(app);
const axios = require('axios');

const port = process.env.PORT || 5001;

server.listen(port);

//Models for MongoDB
const Devices = require('./models/device');
const Accounts = require('./models/user');
const CloudManager = require('./manager');

mongoose.connect(process.env.MONGO_URL);

// app.listen(port, () => {
//     console.log(`listening on port ${port}`);
// });

app.use(bodyParser.json({ limit: '1000mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '1000mb' }));
app.use(bodyParser.raw({ limit: '1GB' }));

// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
//     next();
// });
app.use(cors());
app.options('*', cors());

app.get('/api/test', (req, res) => {
    return res.json({
        success: true,
        message: 'pong'
    })
});

app.get('/api/devices/:api_key', async(req, res) => {
    const { api_key } = req.params;

    const username = await CloudManager.getUsername(api_key);

    Devices.find({ username: username }, (err, document) => {
        if (document != undefined) {
            return res.send(document);
        }

        return res.send('[]');
    });
});

app.get('/api/users/:api_key', (req, res) => {
    const { api_key } = req.params;

    Accounts.findOne({ api_key }, (err, document) => {
        if (document == undefined || document == null) {
            return res.json({
                success: false,
                message: 'This user does not have information.'
            });
        }

        return res.json({
            success: true,
            email: document.email,
            name: document.name
        });
    });
});

app.post('/api/devices', async(req, res) => {
    const { deviceId, apiKey, deviceName, ip, type } = req.body;

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    if (await CloudManager.deviceIdExists(deviceId, true)) {
        return res.json({
            success: false,
            message: 'Device is already registered.',
            device_id: CloudManager.getDeviceId(await CloudManager.getUsername(apiKey), deviceName)
        });
    }

    if (await CloudManager.registerDevice(deviceId, deviceName, await CloudManager.getUsername(apiKey), ip, type, new Date().getDate(), false)) {
        return res.json({
            success: true,
            message: 'Device has been registered.',
            device_id: await CloudManager.getDeviceId(await CloudManager.getUsername(apiKey), deviceName)
        });
    } else {
        return res.json({
            success: false,
            message: 'Failed to register device.'
        });
    }
});

app.put('/api/status/:deviceId/:apiKey', async(req, res) => {
    const { deviceId, apiKey } = req.params;
    console.log(req.params);

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    if (await CloudManager.deviceIdExists(deviceId, false) == false) {
        return res.json({
            success: false,
            message: 'Device does not exist.'
        });
    }

    if (await CloudManager.updateDeviceStatus(deviceId, false)) {
        return res.json({
            success: true,
            message: 'Device status has been updated.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to update device status.'
    });
});

app.put('/api/devices', async(req, res) => {
    const { deviceId, apiKey, ip, last_accessed, device_name } = req.body;

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    if (await CloudManager.deviceIdExists(deviceId, true) == false) {
        return res.json({
            success: false,
            message: 'Device does not exist.'
        });
    }

    if (await CloudManager.updateDevice(deviceId, ip, last_accessed, device_name)) {
        return res.json({
            success: true,
            message: 'Device has been updated.'
        });
    } else {
        return res.json({
            success: false,
            message: 'Failed to update device details.'
        });
    }
});

app.post('/api/authenticate', (req, res) => {
    const { email, password } = req.body;
    Accounts.findOne({ "email": email }, (err, users) => {
        if (err == true) {
            return res.send(err);
        } else if (users == undefined) {
            return res.json({
                message: 'User does not exist',
                success: false
            });
        } else if (password != users.password) {
            return res.json({
                message: 'Incorrect Password !',
                success: false
            });
        } else {
            return res.json({
                success: true,
                message: 'Authenticated successfully',
                key: users.api_key
            });
        }
    });
});

app.post('/api/registeration', (req, res) => {
    const { name, email, password, confirm_password } = req.body;
    Accounts.findOne({ "email": email }, (err, users) => {
        if (err == true) {
            return res.send(err);
        } else if (name.length < 1) {
            return res.json({
                success: false,
                message: 'Your must provide your name.'
            });
        } else if (email.search('@') == -1 || email.length < 7) {
            return res.json({
                success: false,
                message: 'Invalid email address.'
            });
        } else if (password.length < 7) {
            return res.json({
                success: false,
                message: 'Your password must contain 7 characters.'
            });
        } else if (users != undefined) {
            return res.json({
                success: false,
                message: 'User already exists... Please log in.'
            });
        } else if (password != confirm_password) {
            return res.json({
                message: 'Incorrect Password !',
                success: false
            });
        }
        const newUser = new Accounts({
            api_key: CloudManager.hash(email, CloudManager.hash(email, name + password + email)),
            name: name,
            email: email,
            password: password,
        });

        newUser.save(err => {
            return err ?
                res.send(err) :
                res.json({
                    success: true,
                    message: 'Created new user'
                });
        });
    });
});

app.get('/api/device/:apiKey/:deviceName', async(req, res) => {
    const { apiKey, deviceName } = req.params;

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    const username = await CloudManager.getUsername(apiKey);
    const id = await CloudManager.getDeviceId(username, deviceName);

    return res.json({
        success: true,
        id: id,
    });
});

app.delete('/api/devices', async(req, res) => {
    const { deviceId, apiKey } = req.body;

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    if (await CloudManager.deviceIdExists(deviceId, false) == false) {
        return res.json({
            success: false,
            message: 'Device doesnt exist.'
        });
    }

    if (await CloudManager.deleteDevice(deviceId)) {
        return res.json({
            success: true,
            message: 'Device has been deleted.'
        });
    } else {
        return res.json({
            success: false,
            message: 'Failed to delete the device.'
        });
    }
});

app.post('/api/settings', async(req, res) => {
    const { settings, password, apiKey } = req.body;

    console.log(req.body);

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

});


app.post('/api/download', async(req, res) => {
    const { file_name, contents, apiKey } = req.body;

    const raw_contents = new Buffer(contents, 'base64');

    console.log(raw_contents.toString('utf-8'));

    if (await CloudManager.apiKeyExists(apiKey) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    if (await CloudManager.saveDownload(apiKey, file_name, raw_contents)) {
        axios.get(`https://rberry-sockets.herokuapp.com/download?key=${apiKey}`)
            .then(response => {
                console.log(response.data);
            })
            .catch(error => {
                console.log(error);
            });

        return res.json({
            success: true,
            message: 'File uploaded successfully for download.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to upload file for download.'
    });
});

app.get('/api/download', async(req, res) => {
    const { key } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {

        return res.json({
            success: false,
            message: 'Invalid Key.'
        });
    }

    const file = await CloudManager.getDownload(key);
    if (file != null) {
        res.setHeader('Content-disposition', 'attachment; filename=' + file.file_name);
        res.setHeader('Content-Type', 'application/octet-stream');

        //data = data.substring(2, data.length - 1);

        return res.send(file.contents);
    }

    return res.json({
        success: false,
        message: 'Failed to download file.'
    });
});