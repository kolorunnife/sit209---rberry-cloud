// CHARTS \\
Chart.pluginService.register({
    beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
            //Get ctx from string
            var ctx = chart.chart.ctx;

            //Get options from the center object in options
            var centerConfig = chart.config.options.elements.center;
            var fontStyle = centerConfig.fontStyle || 'Arial';
            var txt = centerConfig.text;
            var color = centerConfig.color || '#000';
            var sidePadding = centerConfig.sidePadding || 20;
            var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
            //Start with a base font of 30px
            ctx.font = "30px " + fontStyle;

            //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
            var stringWidth = ctx.measureText(txt).width;
            var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

            // Find out how much the font can grow in width.
            var widthRatio = elementWidth / stringWidth;
            var newFontSize = Math.floor(30 * widthRatio);
            var elementHeight = (chart.innerRadius * 2);

            // Pick a new font size so it will not be larger than the height of label.
            var fontSizeToUse = Math.min(newFontSize, elementHeight);

            //Set font settings to draw it correctly.
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
            var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
            ctx.font = fontSizeToUse + "px " + fontStyle;
            ctx.fillStyle = color;

            //Draw text in center
            ctx.fillText(txt, centerX, centerY);
        }
    }
});

var ramChart = new Chart($("#ramUsageChart"), {
    type: 'doughnut',
    data: {
        labels: ['Ram Usage'],
        datasets: [{
            label: 'Ram Usage',
            data: [1, 90],
            backgroundColor: [
                'rgba(0, 0, 255, 0.35)',
                'rgba(0, 0, 0, 0)']
        }],
    },
    options: {
        responsive: true,
        tooltips: { enabled: false },
        elements: {
            center: {
                text: '1%',
                color: 'rgba(0, 0, 255, 0.35)', 
                fontStyle: 'Arial', 
                sidePadding: 15 
            }
        }
    }
});

var cpuChart = new Chart($("#cpuUsageChart"), {
    type: 'doughnut',
    data: {
        labels: ['Cpu Usage'],
        datasets: [{
            label: 'Cpu Usage',
            data: [1, 90],
            backgroundColor: [
                'rgba(0, 255, 0, 0.35)',
                'rgba(0, 0, 0, 0)']
        }],
    },
    options: {
        responsive: true,
        tooltips: { enabled: false },
        elements: {
            center: {
                text: '1%',
                color: 'rgba(0, 255, 0, 0.35)', 
                fontStyle: 'Arial', 
                sidePadding: 15
            }
        }
    }
});

var tempChart = new Chart($("#tempUsageChart"), {
    type: 'doughnut',
    data: {
        labels: ['Core Temp'],
        datasets: [{
            label: 'Core Temp',
            data: [50, 50],
            backgroundColor: [
                'rgba(255, 0, 0, 0.35)',
                'rgba(0, 0, 0, 0)']
        }],
    },
    options: {
        responsive: true,
        tooltips: { enabled: false },
        elements: {
            center: {
                text: '50C',
                color: 'rgba(255, 0, 0, 0.35)', 
                fontStyle: 'Arial', 
                sidePadding: 50 
            }
        }
    }
});

function SetChartPercent(chart, data, suffix) {
    if (data == 0) return;

    var dataset = chart.data.datasets[0];
    dataset.data[0] = data;
    dataset.data[1] = 100 - data;
    chart.options.elements.center.text = data + suffix;
    chart.update();
}