const mongoose = require('mongoose');

module.exports = mongoose.model('Devices', new mongoose.Schema({
    device_id: String,
    name: String,
    type: String,
    username: String,
    ip_address: String,
    last_accessed: String,
    status: Boolean
}));
