const Devices = require('./models/device');
const crypto = require('crypto');
const http = require('http');
const axios = require('axios');

module.exports = {

    //Misc
    hash: function (secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    //Device
    deviceIdExists: async function (key) {
        var promise = Devices.findOne({ device_id: key.trim() }, (err, document) => {

            if (err) {
                console.log(err);
            }

            if (document == null || document == undefined) {
                //console.log('Device does not exist. -> ' + result);
            }
            else {
                //console.log('Device does exist. -> ' + result);
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    updateDeviceStatus: async function (device_id, key, status) {

        await axios.put(`https://rberry-api.herokuapp.com/api/status/${device_id}/${key}`)
            .then(response => {
                console.log(response.data);
            })
            .catch(error => {
                console.log(error);
            });

        // http.put('http://localhost:5001/api/status/' + device_id + '/' + key, (resp) => {
        //     let data = '';

        //     // A chunk of data has been recieved.
        //     resp.on('data', (chunk) => {
        //         data += chunk;
        //     });

        //     // The whole response has been received. Print out the result.
        //     resp.on('end', () => {
        //         console.log(data);
        //     });

        // }).on("error", (err) => {
        //     console.log("Error: " + err.message);
        // });

    }
}
