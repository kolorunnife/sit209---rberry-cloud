const mongoose = require('mongoose');
const Accounts = require('./models/user');

module.exports = {

    //Misc
    authenicateClient: async function(res, key){
        if (await this.apiKeyExists(key) == false){
            return false;
        }

        return true;
    },
    
    //Users
    apiKeyExists: async function(key) {

        var promise = Accounts.findOne({ api_key: key }, (err, document) => {
            
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? false: true;
    },

    getUsername: async function(key) {
        var result = ' ';

        var promise = Accounts.findOne({ api_key: key }, (err, document) => {
            if (document != undefined && document != null) {
                result = document.email;
            }
        });

        const promiseValue = await promise

        return promiseValue == null || promiseValue == undefined ? ' ': promiseValue.email;
    },
}
