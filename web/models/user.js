const mongoose = require('mongoose');

module.exports = mongoose.model('Accounts', new mongoose.Schema({
    api_key: String,
    name: String,
    password: String,
    email: String
}));
